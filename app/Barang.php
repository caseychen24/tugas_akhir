<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Barang extends Model
{

  public $table = 'tbarang';

  protected $fillable = ['nama_barang','stok','harga'];

}
